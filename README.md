My AH Back
=======
Port d'écoute du back 3000

Compatible node version 10

Pré-requis: 
<pre>
npm install
npm run build
</pre>

Configuration

Variable d'environnement obligatoire : 
-
<pre>
// connection au serveur amqp
AMQP_HOST
AMQP_PORT
AMQP_USER
AMQP_PASSWORD

// connection à la base de données
MONGO_HOST
MONGO_PORT
</pre>


Variable d'environnement facultatif : 
-
<pre>
MONGO_USER
MONGO_PASSWORD
</pre>
