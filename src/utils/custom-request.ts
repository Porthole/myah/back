import { IUser } from '../user/user.document';

export interface Context {
  [k: string]: any;
  user?: IUser;
}

export class CustomRequest extends Request {
  private _context: Context = {};

  get context(): Context {
    return this._context;
  }

  set context(value: Context) {
    this._context = value;
  }
}
