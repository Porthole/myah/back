import { ioInstance } from '../utils/io-instance';
import { SubscribeRequest } from './subscribe-request';
import * as SocketIo from 'socket.io';
import { UnsubscribeRequest } from './unsubscribe-request';
import { SubscribeClient } from './subscribe-client';
import * as _ from 'lodash';
import { configureLogger, defaultWinstonLoggerOptions, getLogger } from '../utils/logger';
import * as Ajv from 'ajv';
import { SubscribeRequestSchema } from './subscribe-request.schema';
import { CustomError, CustomErrorCode } from '../utils/custom-error';

const ajv = new Ajv({ allErrors: true, removeAdditional: true });

const loggerName = 'WebSocketEvent';
configureLogger(loggerName, defaultWinstonLoggerOptions);
const logger = getLogger(loggerName);

export class WebSocketEvent {
  private static instance: WebSocketEvent;
  io: SocketIo.Server;
  listeners: Map<string, SubscribeClient>; // mixed.id -> SubClient
  topicListeners: Map<string, Set<SubscribeClient>>; // topic -> SubClients[]

  static get() {
    if (!this.instance) {
      this.instance = new WebSocketEvent();
    }
    return this.instance;
  }

  private constructor() {
    this.io = ioInstance();
    this.listeners = new Map<string, SubscribeClient>();
    this.topicListeners = new Map<string, Set<SubscribeClient>>();
    this.initSocketIo();
  }

  private initSocketIo(): void {
    this.io.on('connection', socket => {
      this.listenSubscribeEvent(socket);
      this.listenUnsubscribeEvent(socket);
      this.onDisconnect(socket);
    });
  }

  private listenSubscribeEvent(socket: SocketIo.Socket): void {
    socket.on('subscribe', (subRequest: SubscribeRequest, callback) => {
      try {
        this.addSubscribe(socket, subRequest);
      } catch (e) {
        callback(e);
      }
    });
  }

  private listenUnsubscribeEvent(socket: SocketIo.Socket): void {
    socket.on('unsubscribe', (unsubRequest: UnsubscribeRequest, callback) => {
      try {
        this.removeSubscribe(socket.id, unsubRequest.topic);
      } catch (e) {
        callback(e);
      }
    });
  }

  public sendMessage(topic: string, msg: any, userId?: string): void {
    let emit = 0;
    for (const topicListener of this.topicListeners) {
      if (topic === topicListener[0]) {
        this.topicListeners.get(topicListener[0]).forEach(listener => {
          if (userId) {
            if (listener.userId === userId) {
              this._emit(listener.socket, topic, msg);
              emit++;
            }
          } else {
            emit++;
            this._emit(listener.socket, topic, msg);
          }
        });
      }
    }
    logger.log('info', emit + ' message(s) emitted');
  }

  private _emit(socket: SocketIo.Socket, topic: string, msg: any) {
    logger.log('info', 'Message emitted to ' + socket.id + 'on ' + topic + ' : ' + JSON.stringify(msg));
    socket.emit(topic, msg);
  }

  private addSubscribe(socket: SocketIo.Socket, subRequest: SubscribeRequest) {
    if (!ajv.validate(SubscribeRequestSchema, subRequest)) {
      logger.log('warn', 'Bad format subscription receive');
      logger.log('warn', JSON.stringify(subRequest));
      throw new CustomError(CustomErrorCode.ERRBADREQUEST, 'Bad format of subscription receive');
    }
    const sub = new SubscribeClient(subRequest, socket);

    if (!this.topicListeners.has(subRequest.topic)) {
      this.topicListeners.set(subRequest.topic, new Set<SubscribeClient>());
    }

    if (!this.listeners.has(sub.id)) {
      this.topicListeners.get(subRequest.topic).add(sub);
      this.listeners.set(sub.id, sub);
      logger.log('info', 'New subscriber n°' + sub.id + ' on ' + sub.topic);
      return;
    }
    throw new CustomError(
      CustomErrorCode.ERRFORBIDDEN,
      'This socket has already subscribe to this topic. Did you forget to unsubscribe the old one ?'
    );
  }

  private removeSubscribe(socketId: string, topic: string) {
    const id = SubscribeClient.getId(socketId, topic);
    const sub = this.listeners.get(id);
    if (sub) {
      const topic = sub.topic;

      this.topicListeners.get(topic).delete(sub);
      this.listeners.delete(id);

      if (this.topicListeners.get(topic).size === 0) {
        this.topicListeners.delete(topic);
      }
      logger.log('info', 'Removing subscriber n°' + sub.id + ' on ' + sub.topic);
      return;
    }
    throw new CustomError(CustomErrorCode.ERRNOTFOUND, 'No listener found for this topic and socket');
  }

  private onDisconnect(socket: SocketIo.Socket): void {
    socket.on('disconnect', reason => {
      logger.log('warn', 'Socket ' + socket.id + ' has been deconnected for ' + reason);
      this.listeners.forEach(listener => {
        if (listener.socket.id === socket.id) {
          const splitId = listener.id.split('_');
          this.removeSubscribe(splitId[0], splitId[1]);
        }
      });
    });
  }
}
