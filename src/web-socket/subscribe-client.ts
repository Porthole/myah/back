import { SubscribeRequest } from './subscribe-request';
import * as SocketIO from 'socket.io';

export class SubscribeClient {
  id: string;
  socket: SocketIO.Socket;
  topic: string;
  userId: string;

  constructor(subRequest: SubscribeRequest, socket: SocketIO.Socket) {
    this.id = SubscribeClient.getId(socket.id, subRequest.topic);
    this.socket = socket;
    this.topic = subRequest.topic;
    this.userId = subRequest.userId;
  }

  public static getId(socketId: string, subId: string) {
    return socketId + '_' + subId;
  }
}
