import * as express from 'express';
import { AuctionatorController } from './auctionator.controller';
import { Validator } from 'express-json-validator-middleware';
import { AuctionatorQuerySchema } from './auctionator.schemas';
import { RouterManager } from '../utils/router.manager';
import { internalAuthenticationMiddleware } from '../middlewares/internal-authentication.middleware';
import { internalAuthorizationMiddleware } from '../middlewares/internal-authorization.middleware';
import { typeFilter } from '../helpers/file-filter.helper';
import multer = require('multer');

import rateLimit = require('express-rate-limit');

const router: express.Router = express.Router();
const validator = new Validator({ allErrors: true, removeAdditional: true });
const auctionatorController = new AuctionatorController();

const upload = multer({ dest: '/tmp/', fileFilter: typeFilter });

const uploadLimiter = rateLimit({
  windowMs: 60 * 1000,
  max: 15,
  message: 'Too many upload from this IP'
});

const routerManager = new RouterManager(router);

const resource = 'auctionators';
/**
 * @apiDefine Auctionator Auctionator
 *
 * List of endpoints for managing auctionators.
 */
routerManager
  .route('/auctionators')
  /**
   * @api {get} /auctionators Get auctionator list
   *
   * @apiGroup Auctionator
   *
   * @apiSuccess {array} auctionators
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 200 OK
   *     [{
   *        "_id": "5b179f629fea4000ObjectIdffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }]
   */
  .get({
    handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, auctionatorController.getAll],
    resource,
    action: 'read'
  })
  /**
   * @api {post} /auctionators Create auctionator
   *
   * @apiGroup Auctionator
   *
   * @apiParam {String} resource,
   * @apiParam {String} actionsAvailable
   *
   * @apiSuccess {String} resource
   * @apiSuccess {String} id ObjectId
   * @apiSuccess {String[]} actionsAvailable
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 201 Created
   *     {
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }
   */
  .post({
    handlers: [
      uploadLimiter,
      upload.fields([{ name: 'data', maxCount: 1 }]),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      auctionatorController.create.bind(auctionatorController)
    ],
    resource,
    action: 'create'
  });

routerManager.route('/auctionators/:id/analyse').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, auctionatorController.analyse],
  resource,
  action: 'read'
});

routerManager.route('/auctionators/:id/recipes').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, auctionatorController.recipeCheck],
  resource,
  action: 'read'
});

routerManager.route('/realms').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, auctionatorController.realms],
  resource,
  action: 'read'
});
routerManager.route('/factions').get({
  handlers: [internalAuthenticationMiddleware, internalAuthorizationMiddleware, auctionatorController.factions],
  resource,
  action: 'read'
});

routerManager
  .route('/auctionators/:id')
  /**
   * @api {get} /auctionators/:id Get auctionator
   *
   * @apiGroup Auctionator
   *
   * @apiParam {String} id ObjectId
   *
   * @apiSuccess {String} name
   * @apiSuccess {String} id ObjectId
   *
   * @apiSuccessExample {json} Success response
   *     HTTP/1.1 200 OK
   *     {
   *        "_id": "5b179f629fea4000ffcf2fbc",
   *        "resource": "user",
   *        "action":["find"]
   *    }
   *
   * @apiErrorExample {json} Error auctionator not found
   *     HTTP/1.1 404 Not Found
   *     {
   *       "error": {
   *          "code": "ERRNOTFOUND",
   *          "message": "Not found"
   *       }
   *     }
   */
  .get({
    handlers: [
      validator.validate({
        params: AuctionatorQuerySchema
      }),
      internalAuthenticationMiddleware,
      internalAuthorizationMiddleware,
      auctionatorController.get
    ],
    resource,
    action: 'read'
  });

export default router;
