import { NextFunction, Request, Response } from 'express';
import { CustomRequest } from '../utils/custom-request';
import { ItemService } from './item.service';
import * as httpStatus from 'http-status';
import * as mongoose from 'mongoose';
import { CustomError, CustomErrorCode } from '../utils/custom-error';
import ObjectId = mongoose.Types.ObjectId;

export class ItemController {
  create(req: Request, res: Response, next: NextFunction): void {
    ItemService.get()
      .create(req.body)
      .then(item => {
        res.status(httpStatus.CREATED).send(item);
      })
      .catch(next);
  }

  getAll(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    ItemService.get()
      .getAll(req.query, customReq.context.skip, customReq.context.limit)
      .then(items => {
        res.status(httpStatus.OK).send(items);
      })
      .catch(next);
  }

  search(req: Request, res: Response, next: NextFunction): void {
    const customReq = (req as any) as CustomRequest;
    ItemService.get()
      .getAll(
        { name: new RegExp('.*' + req.query.name + '.*', 'gmi') },
        customReq.context.skip,
        customReq.context.limit
      )
      .then(items => {
        res.status(httpStatus.OK).send(items);
      })
      .catch(next);
  }

  get(req: Request, res: Response, next: NextFunction): void {
    ItemService.get()
      .get(new ObjectId(req.params.id), req.query)
      .then(item => {
        if (!item) {
          throw new CustomError(CustomErrorCode.ERRNOTFOUND, ' Item not found');
        }
        res.status(httpStatus.OK).send(item);
      })
      .catch(next);
  }

  update(req: Request, res: Response, next: NextFunction): void {
    ItemService.get()
      .update(new ObjectId(req.params.id), req.body)
      .then(item => {
        res.status(httpStatus.OK).send(item);
      })
      .catch(next);
  }
}
