import { CustomError, CustomErrorCode } from '../utils/custom-error';

export function typeFilter(req: any, file: any, cb: any) {
  if (!file.originalname.match(/\.(lua|LUA)$/)) {
    req.fileValidationError = 'Format not authorised';
    return cb(new CustomError(CustomErrorCode.ERRBADREQUEST, 'Format not authorised'));
  }
  cb(null, true);
}
