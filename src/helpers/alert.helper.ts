import { CustomError } from '../utils/custom-error';

export class Alert {
  title?: string;
  message: string;
  type: AlertType;
  data?: any;
}

export enum AlertType {
  Success = 'success',
  Error = 'error',
  Info = 'info',
  Warning = 'warn'
}

export class AlertHelper {
  static fromCustomError(error: CustomError): Alert {
    return {
      type: AlertType.Error,
      title: 'Error',
      message: error.message,
      data: {
        cause: error.cause
      }
    };
  }
}
