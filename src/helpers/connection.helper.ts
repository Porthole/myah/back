import { getConfiguration } from './configuration.helper';
import { getPackageName } from './package.helper';

const appName = getPackageName();

export function getDatabaseConnectionUrl() {
  const config: any = getConfiguration();
  if (!config[appName].databaseName) {
    return null;
  }

  let url = 'mongodb://';

  if (process.env.MONGO_USER && process.env.MONGO_PASSWORD) {
    url += process.env.MONGO_USER + ':' + process.env.MONGO_PASSWORD + '@';
  }
  //
  // if (!(config[appName] && config[appName].databases)) {
  //   return null;
  // }

  // for (const database of config[appName].databases) {
    url += `${process.env.MONGO_HOST}:${process.env.MONGO_PORT},`;
  // }

  return url.substr(0, url.length - 1) + `/${config[appName].databaseName}`;
}
