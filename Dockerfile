FROM node:10

WORKDIR /opt/back
COPY . .
RUN npm install
RUN npm run build
EXPOSE 3000

CMD npm run start
